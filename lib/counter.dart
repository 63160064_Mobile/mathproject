import 'package:flutter/widgets.dart';

class CountingTheNumber with ChangeNotifier {
  int score = 0;

  void increaseScore() {
    score += 100;
    notifyListeners();
  }

  void decreaseScore() {
    score -= 50;
    if (score < 0) {
      score = 0;
    }
    notifyListeners();
  }

  void resetScore(){
    score = 0;
    notifyListeners();
  }
}
