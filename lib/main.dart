import 'dart:async';
import 'dart:math';

import 'package:device_preview/device_preview.dart';
import 'package:flutter/material.dart';
import 'package:math_expressions/math_expressions.dart';
import 'package:provider/provider.dart';

import 'counter.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return DevicePreview(
      tools: const [
        DeviceSection(),
      ],
      builder: (context) => MaterialApp(
          debugShowCheckedModeBanner: false,
          useInheritedMediaQuery: true,
          builder: DevicePreview.appBuilder,
          locale: DevicePreview.locale(context),
          title: 'Responsive and adaptive UI in Flutter',
          theme: ThemeData(
            scaffoldBackgroundColor: Colors.pinkAccent.shade100,
            primarySwatch: Colors.green,
          ),
          home: ChangeNotifierProvider<CountingTheNumber>(
            create: (BuildContext context) => CountingTheNumber(),
            child: const RestartWidget(child: SpeedMath(title: "คิดเลขเร็ว")),
          )
          // SpeedMath(title: "คิดเลขเร็ว", score: 0,),
          ),
    );
  }
}

class RestartWidget extends StatefulWidget {
  const RestartWidget({super.key, this.child});

  final Widget? child;

  static void restartApp(BuildContext context) {
    context.findAncestorStateOfType<_RestartWidgetState>()?.restartApp();
  }

  @override
  State<StatefulWidget> createState() {
    return _RestartWidgetState();
  }
}

class _RestartWidgetState extends State<RestartWidget> {
  Key key = UniqueKey();

  void restartApp() {
    setState(() {
      key = UniqueKey();
    });
  }

  @override
  Widget build(BuildContext context) {
    return KeyedSubtree(
      key: key,
      child: widget.child ?? Container(),
    );
  }
}

class SpeedMath extends StatefulWidget {
  const SpeedMath({super.key, required this.title});

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  State<SpeedMath> createState() => _SpeedMathState();
}

class _SpeedMathState extends State<SpeedMath> {
  static const maxSeconds = 60;
  int seconds = maxSeconds;
  Timer? timer;
  int _question = 0;
  int _num1 = 0;
  int _num2 = 0;
  int _num3 = 0;
  int _num4 = 0;
  bool nowNum = false;
  bool btn1Select = false;
  bool btn2Select = false;
  bool btn3Select = false;
  bool btn4Select = false;

  void startTimer(CountingTheNumber counter) {
    timer = Timer.periodic(const Duration(seconds: 1), (_) {
      if (seconds > 0) {
        setState(() => seconds--);
      } else {
        timer?.cancel();
        showDialog(
            context: context,
            barrierDismissible: false,
            builder: (BuildContext dialog) {
              return AlertDialog(
                title: const Text('Game Over'),
                content: SingleChildScrollView(
                    child: Text('Your score: ${counter.score}')),
                actions: [
                  TextButton(
                    child: const Text(
                      'Restart',
                      style: TextStyle(color: Colors.pink, fontSize: 16),
                    ),
                    onPressed: () {
                      counter.resetScore();
                      RestartWidget.restartApp(context);
                      Navigator.pop(dialog);
                    },
                  )
                ],
              );
            });
      }
    });
  }

  void _initQuiz() {
    setState(() {
      while (_question <= 0 || _question > 100) {
        _num1 = Random().nextInt(9) + 1;
        _num2 = Random().nextInt(9) + 1;
        _num3 = Random().nextInt(9) + 1;
        _num4 = Random().nextInt(9) + 1;
        int _ope1 = Random().nextInt(4) + 1;
        int _ope2 = Random().nextInt(4) + 1;
        int _ope3 = Random().nextInt(4) + 1;
        _question = _num1;
        print(_num1);
        if (_ope1 == 1) {
          _question += _num2;
          print('+ $_num2');
        } else if (_ope1 == 2) {
          _question -= _num2;
          print('- $_num2');
        } else if (_ope1 == 3) {
          _question *= _num2;
          print('* $_num2');
        } else if (_ope1 == 4) {
          //divide is for double so can't assign to int and it's too hard for kids.
          //_question /= _num2;
          _question *= _num2;
          print('* $_num2');
        }

        if (_ope2 == 1) {
          _question += _num3;
          print('+ $_num3');
        } else if (_ope2 == 2) {
          _question -= _num3;
          print('- $_num3');
        } else if (_ope2 == 3) {
          _question *= _num3;
          print('* $_num3');
        } else if (_ope2 == 4) {
          _question *= _num3;
          print('* $_num3');
        }

        if (_ope3 == 1) {
          _question += _num4;
          print('+ $_num4');
        } else if (_ope3 == 2) {
          _question -= _num4;
          print('- $_num4');
        } else if (_ope3 == 3) {
          _question *= _num4;
          print('* $_num4');
        } else if (_ope3 == 4) {
          _question *= _num4;
          print('* $_num4');
        }

        //random swap
        List<int> numbers = [_num1, _num2, _num3, _num4];
        int numSwaps = Random().nextInt(9) + 1;

        for (int i = 0; i < numSwaps; i++) {
          int index1 = Random().nextInt(numbers.length);
          int index2 = Random().nextInt(numbers.length);

          // Swap elements at index1 and index2
          int temp = numbers[index1];
          numbers[index1] = numbers[index2];
          numbers[index2] = temp;
        }

        _num1 = numbers[0];
        _num2 = numbers[1];
        _num3 = numbers[2];
        _num4 = numbers[3];
      }
    });
  }

  final _answer = TextEditingController();

  void _addText(text) {
    setState(() {
      _answer.text += text;
    });
  }

  void _delText() {
    setState(() {
      if (_answer.text.isNotEmpty) {
        try {
          if (int.parse(_answer.text.substring(
                      _answer.text.length - 1, _answer.text.length)) ==
                  _num1 &&
              btn1Select) {
            btn1Select = false;
          } else if (int.parse(_answer.text.substring(
                      _answer.text.length - 1, _answer.text.length)) ==
                  _num2 &&
              btn2Select) {
            btn2Select = false;
          } else if (int.parse(_answer.text.substring(
                      _answer.text.length - 1, _answer.text.length)) ==
                  _num3 &&
              btn3Select) {
            btn3Select = false;
          } else if (int.parse(_answer.text.substring(
                      _answer.text.length - 1, _answer.text.length)) ==
                  _num4 &&
              btn4Select) {
            btn4Select = false;
          }
          nowNum = false;
        } catch (e) {
          if (_answer.text.substring(
                      _answer.text.length - 1, _answer.text.length) !=
                  "(" &&
              _answer.text.substring(
                      _answer.text.length - 1, _answer.text.length) !=
                  ")") {
            nowNum = true;
          }
        }
        _answer.text = _answer.text.substring(0, _answer.text.length - 1);
      }
    });
  }

  int evaluateExpression(String expression) {
    String replacedexpression = expression.replaceAll('x', '*');

    // Create a parser
    final parser = Parser();

    // Parse the expression
    final parsedExpression = parser.parse(replacedexpression);

    // Evaluate the expression
    final result = parsedExpression
        .evaluate(EvaluationType.REAL, ContextModel())
        .toDouble();

    // Return the integer value
    print(result.toInt());
    return result.toInt();
  }

  @override
  Widget build(BuildContext context) {
    _initQuiz();
    final counter = Provider.of<CountingTheNumber>(context);
    timer ?? startTimer(counter);

    return Scaffold(
      body: SafeArea(
        child: ListView(
          children: [
            Center(
              child: ConstrainedBox(
                constraints: const BoxConstraints(maxWidth: 300),
                child: Column(
                  children: [
                    Text('Score: ${counter.score}',
                        style: const TextStyle(fontSize: 24)),
                    const SizedBox(height: 15),
                    Container(
                      width: 250,
                      height: 100,
                      decoration: BoxDecoration(
                          color: Colors.yellow.shade300,
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey.withOpacity(0.5),
                              spreadRadius: 5,
                              blurRadius: 7,
                              // offset: Offset(4,8),
                            )
                          ]),
                      child: Align(
                        alignment: Alignment.center,
                        child: Text(
                          '$_question',
                          style: const TextStyle(fontSize: 32),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ),
                    const SizedBox(height: 30),

                    //ปุ่ม + - x /
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            backgroundColor: Colors.pink,
                            fixedSize: const Size.fromWidth(100),
                            textStyle: const TextStyle(fontSize: 24),
                          ),
                          onPressed: () {
                            if (nowNum) {
                              nowNum = false;
                              _addText('+');
                            }
                          },
                          child: const Text('+'),
                        ),
                        const SizedBox(width: 50),
                        ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            backgroundColor: Colors.pink,
                            fixedSize: const Size.fromWidth(100),
                            textStyle: const TextStyle(fontSize: 24),
                          ),
                          onPressed: () {
                            if (nowNum) {
                              nowNum = false;
                              _addText('-');
                            }
                          },
                          child: const Text('-'),
                        ),
                      ],
                    ),
                    const SizedBox(height: 30),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            backgroundColor: Colors.pink,
                            fixedSize: const Size.fromWidth(100),
                            textStyle: const TextStyle(fontSize: 24),
                          ),
                          onPressed: () {
                            if (nowNum) {
                              nowNum = false;
                              _addText('x');
                            }
                          },
                          child: const Text('x'),
                        ),
                        const SizedBox(width: 50),
                        ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            backgroundColor: Colors.pink,
                            fixedSize: const Size.fromWidth(100),
                            textStyle: const TextStyle(fontSize: 20),
                          ),
                          onPressed: () {
                            if (nowNum) {
                              nowNum = false;
                              _addText('/');
                            }
                          },
                          child: const Text('/'),
                        ),
                      ],
                    ),
                    const SizedBox(height: 30),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            backgroundColor: Colors.pink,
                            fixedSize: const Size.fromWidth(100),
                            textStyle: const TextStyle(fontSize: 24),
                          ),
                          onPressed: () {
                            _addText('(');
                          },
                          child: const Text('('),
                        ),
                        const SizedBox(width: 50),
                        ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            backgroundColor: Colors.pink,
                            fixedSize: const Size.fromWidth(100),
                            textStyle: const TextStyle(fontSize: 20),
                          ),
                          onPressed: () {
                            _addText(')');
                          },
                          child: const Text(')'),
                        ),
                      ],
                    ),
                    const SizedBox(height: 30),

                    //ปุ่มกดตัวเลข
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            foregroundColor: Colors.black,
                            backgroundColor: Colors.white,
                            fixedSize: const Size.fromWidth(100),
                            textStyle: const TextStyle(fontSize: 24),
                          ),
                          onPressed: () {
                            if (!btn1Select && !nowNum) {
                              btn1Select = true;
                              nowNum = true;
                              _addText('$_num1');
                            }
                          },
                          child: Text('$_num1'),
                        ),
                        const SizedBox(width: 50),
                        ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            foregroundColor: Colors.black,
                            backgroundColor: Colors.white,
                            fixedSize: const Size.fromWidth(100),
                            textStyle: const TextStyle(fontSize: 24),
                          ),
                          onPressed: () {
                            if (!btn2Select && !nowNum) {
                              btn2Select = true;
                              nowNum = true;
                              _addText('$_num2');
                            }
                          },
                          child: Text('$_num2'),
                        ),
                      ],
                    ),
                    const SizedBox(height: 30),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            foregroundColor: Colors.black,
                            backgroundColor: Colors.white,
                            fixedSize: const Size.fromWidth(100),
                            textStyle: const TextStyle(fontSize: 24),
                          ),
                          onPressed: () {
                            if (!btn3Select && !nowNum) {
                              btn3Select = true;
                              nowNum = true;
                              _addText('$_num3');
                            }
                          },
                          child: Text('$_num3'),
                        ),
                        const SizedBox(width: 50),
                        ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            foregroundColor: Colors.black,
                            backgroundColor: Colors.white,
                            fixedSize: const Size.fromWidth(100),
                            textStyle: const TextStyle(fontSize: 24),
                          ),
                          onPressed: () {
                            if (!btn4Select && !nowNum) {
                              btn4Select = true;
                              nowNum = true;
                              _addText('$_num4');
                            }
                          },
                          child: Text('$_num4'),
                        ),
                      ],
                    ),
                    const SizedBox(height: 20),

                    //ช่องคำตอบ
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        // SizedBox(width: 50),
                        SizedBox(
                          width: 180,
                          // height: 40,
                          child: TextField(
                            readOnly: true,
                            enabled: false,
                            controller: _answer,
                            decoration: const InputDecoration(
                              border: OutlineInputBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(20))),
                              isDense: true,
                              filled: true,
                              fillColor: Colors.white,
                              contentPadding: EdgeInsets.symmetric(
                                  vertical: 5, horizontal: 10),
                            ),
                            style: const TextStyle(fontSize: 20),
                            textAlignVertical: TextAlignVertical.center,
                          ),
                        ),
                        IconButton(
                          padding: EdgeInsets.zero,
                          constraints: const BoxConstraints(),
                          iconSize: 50,
                          onPressed: () {
                            _delText();
                          },
                          icon: const Icon(Icons.close),
                        ),
                        IconButton(
                          padding: EdgeInsets.zero,
                          constraints: const BoxConstraints(),
                          iconSize: 60,
                          onPressed: () {
                            if (btn1Select &&
                                btn2Select &&
                                btn3Select &&
                                btn4Select) {
                              if (evaluateExpression(_answer.text) ==
                                  _question) {
                                print("correct");
                                timer?.cancel();
                                counter.increaseScore();
                                RestartWidget.restartApp(context);
                              } else {
                                print("incorrect");
                                final snackBar = SnackBar(
                                  content: const Text(
                                    'Incorrect',
                                    style: TextStyle(
                                      color: Colors.red,
                                    ),
                                  ),
                                  action: SnackBarAction(
                                    label: 'Close',
                                    textColor: Colors.red,
                                    onPressed: () {},
                                  ),
                                  duration: Duration(seconds: 2),
                                );
                                ScaffoldMessenger.of(context)
                                    .showSnackBar(snackBar);
                                counter.decreaseScore();
                              }
                            }
                          },
                          icon: const Icon(Icons.navigate_next),
                        ),
                      ],
                    ),
                    const SizedBox(height: 10.0),
                    Text(
                      '$seconds',
                      style: const TextStyle(fontSize: 32),
                    ),
                    const SizedBox(height: 10.0),
                    Image.network(
                      "https://media.discordapp.net/attachments/793093052797419520/1082535504400089150/2-removebg-preview.png?width=322&height=356",
                      height: 150,
                      width: 300,
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
